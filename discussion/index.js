// Mathematical Operatos




/*
	+ Sum
	- Difference
	* Product
	/ Division
	% Modulo


	used when the value of the first number is stored in a variable. performs

	+= Dddition
	-= Subtraction
	*= Multiplication
	/= Division
	%= Modulo

*/



function mod(){
	// return 9-2;
	// return 9*2;
	// return 9/2;

	/*let x = 10;
	return x +=2
	return x -=2
	return x *=2
	return x /=2*/

	return 9%2

	

};


console.log(mod());


// Assignment Operator (=) is used for assigning values to variables
let x=1;
let sum = 1;
// sum = sum + 1
x-=1;
sum +=1;

console.log(sum);
console.log(x);



// increment & decrement


// Increment

// Pre-increment - add 1 before assignning the value to the variable
let z = 1
// let z = (1) + 4
let increment = ++z;

console.log(`Result of pre-icrement: ${increment}`);
console.log(`Result of pre-icrement: ${z}`);


// Post-increment - adding 1 AFTER assigning the value to the variable
increment = z++;
console.log(`Result of post-icrement: ${increment}`);
console.log(`Result of post-icrement: ${z}`);


// Decrement

// pre-decrement - -1 before the assigning of values (basing from the most recent value of z)
let decrement = --z
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);


// post-decrement
decrement = z--
console.log(`Result of post-decrement: ${decrement}`);
console.log(`Result of post-decrement: ${z}`);


// Comparison Operators
// Equality Operator (==) - compares if the two values are equal
let juan = "juan";

console.log(1==1);
console.log(0==false);

console.log(juan == "jupan");


// Strict Equality (===)
// use case = navigating in the webpages Course, Course Outline
/*// if (Course = Course){
	course.html
}
*/
/*// if (Data == Course Outline){
	courseOutline.html
}
*/



// the javascript is not anymore concerned with other relative functions of the data, it only compares the two values as they are
console.log(1===1);


// Inequality (!=)
console.log(1 !=1);
console.log(juan != "juan");

// Strict inequality (!==)
console.log(0 !== false);


// other comparison operators
/*
	> Greater than
	< Less than
	>= Greater than or equal
	<= Less than or equal

*/



// Logical Operators

/*
	AND Operator (&&) - returns true if all operands are true;
	true + true = true
	true + false = false
	false + true = false
	false + false = false

	
	OR Operator (||) - returns true if one of the operands are true
	true + true = true
	true + false = true
	false + true = true
	false + false = false

*/

// AND OPERATOR
let isLegalAge = true;
let isRegistered = true;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`);



// OR OPERATOR
allRequirementMet = isLegalAge || isRegistered
console.log(`Result of logical OR Operator: ${allRequirementMet}`);


// Selection Control Structures

/*
	IF Statements - executes a command if a specified condition is true
		Syntax:
			if (condition){
				statements/command;
			}
*/
let num = -1;
if (num<0){
	console.log("Hello");
}



let value = 30;
if (value > 10 || value < 40){
	console.log("Welcome to Zuitt");
}


/*
IF- ELSE STATEMENT - executes a command if the first condition returns false
	SYNTAX
		if (condition){
			statement if true
		}
		else{
			statement if false
		}
*/


num = 5
if (num > 10){
	console.log(`Number is greater than 10`)
}
else{
	console.log(`number is less than 10`)
};




/*
	prompt - dialog box that has input fields
	alert - dialog box that no input fields; used for warnings, announcements etc
	parseInt - converts number in string data type into numberical data type. it only recognizes numbers and
	ignores the alphabets (it returns Nan - Not a number)
*/
num	= parseInt(prompt("Please input a number"));

if (num > 59){
	alert("Senior Age")
	console.log(num)
}
else{
	alert("Invalid Age")
	console.log("Invalid Age")
};



// IF - ELSEIF-ELSE statement - multiple separate, yet connected conditions
	/*
	SYNTAX


	*/
/*
	1. Quezon
	2. Valenzuela
	3. Pasig
	4. Taguig

*/

let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
	alert("Welcome to Quezon City")
}
else if (city === 2){
	alert("Welcome to Valenzuela City")
}
else if ( city === 3 ){
	alert("Welcome to Pasig City")
}
else if ( city === 4 ){
	alert("Welcome to Taguig City")
}
else{
	alert("Invalid Number")
};




let message = "";

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return	`Not a typhoon yet.`
	}
	else if (windspeed <= 61) {
		return `Tropical Depression detected.`
	}
	else if (windspeed >=62 && windspeed <= 88){
		return `Tropical Storm detected.`
	}
	else if (windspeed >= 89 && windspeed <= 117) {
		return	`Severe Tropical Storm detected`
	}
	else{
		return `Typhoon detected`
	}
};
message = determineTyphoonIntensity(120);
console.log(message);


/*
	Ternary Operator - shorthanded if-else statement
		SYNTAX
			(condition)? ifTrue : ifFalse

*/
let ternaryResult = (1<18)? true:false;
console.log(`Result of ternary operator: ${ternaryResult}`);


// SWITCH STATEMENT - shorthand for if-elseif-else should there be a lot of conditions to be met



let day = prompt("What day is it today?").toLowerCase();
switch (day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};



// Try-Catch-Finally

function showIntensityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntensityAlert(56);